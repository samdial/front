import React, { FC } from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import LobbyPage from "../../../pages/Lobby/ui/LobbyPage";
import ProfilePage from "../../../pages/Profile/ui/ProfilePage";
import { AuthPage } from "../../../pages/Auth/ui/AuthPage";
import { useAuth } from "../../porviders/AuthProvider/ui/AuthContext";

const PrivateRoutes = () => {
  const { isLoggedIn } = useAuth();
  return isLoggedIn ? <Outlet /> : <Navigate to={"/auth"} />;
};

export const AppRouter: FC = () => {
  return (
    <Routes>
      <Route element={<PrivateRoutes />}>
        <Route element={<LobbyPage />} path="/" />
        <Route element={<ProfilePage />} path="/profile" />
      </Route>
      <Route path="/auth" element={<AuthPage />} />
    </Routes>
  );
};
