import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { AUTH_LOCAL_STORAGE_KEY } from "../../../../shared/lib/const/login-consts/login-consts";
import { AuthData } from "../../../../features/Login/model/types/AuthData";
import { useNavigate } from "react-router-dom";

interface AuthContextValue {
  isLoggedIn: boolean;
  login: (username: string, password: string) => void;
  logout: () => void;
  userAuthData?: AuthData;
}

const AuthContext = createContext<AuthContextValue | undefined>(undefined);

export const AuthProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [userAuthData, setUserAuthData] = useState<AuthData | undefined>(
    undefined,
  );
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const userAuthDataJson = localStorage.getItem(AUTH_LOCAL_STORAGE_KEY);

    if (userAuthDataJson) {
      const parsedUserAuthData = JSON.parse(userAuthDataJson) as AuthData;
      setUserAuthData(parsedUserAuthData);
    }
  }, []);

  useEffect(() => {
    if (!userAuthData) {
      navigate("/auth");
    }
  }, [userAuthData]);

  useEffect(() => {
    setIsLoggedIn(Boolean(userAuthData));
  }, [userAuthData]);

  const login = (username: string, password: string) => {
    if (username && password) {
      console.log(
        `Пользователь вошел с именем: ${username} и паролем: ${password}`,
      );
      localStorage.setItem(
        AUTH_LOCAL_STORAGE_KEY,
        JSON.stringify({ username, password }),
      );

      setUserAuthData({
        username,
        password,
      });
    }
  };

  const logout = () => {
    localStorage.removeItem(AUTH_LOCAL_STORAGE_KEY);
    setUserAuthData(undefined);
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn, login, logout, userAuthData }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};
