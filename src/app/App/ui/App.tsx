import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthProvider } from "../../porviders/AuthProvider/ui/AuthContext";
import "./App.css";
import { AppRouter } from "../../AppRouter/ui/AppRouter";
import { Header } from "../../../widgets/Header/ui/Header";

const App: React.FC = () => {
  return (
    <Router>
      <AuthProvider>
        <div className="App">
          <Header />
          <main>
            <AppRouter />
          </main>
        </div>
      </AuthProvider>
    </Router>
  );
};

export default App;
