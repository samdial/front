import React, { useState } from "react";
import { useAuth } from "../../../app/porviders/AuthProvider/ui/AuthContext";
import { useNavigate } from "react-router-dom";
import Input from "../../../shared/ui/Input/ui/Input";
import Button from "../../../shared/ui/Button/ui/Button";

const AuthForm: React.FC = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useAuth();
  const navigate = useNavigate();

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setPassword(event.target.value);
  };

  const handleSubmit = () => {
    login(username.trim(), password);
    navigate("/");
  };

  return (
    <section className="bg-gray-50 dark:bg-gray-900">
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <a
          href="#"
          className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
        >
          <img
            className="w-8 h-8 mr-2"
            src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/logo.svg"
            alt="logo"
          />
          крестики\нолики
        </a>
        <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Sign in to your account
            </h1>
            <form className="space-y-4 md:space-y-6" action="#">
              <div>
                <Input
                  label={"Your username"}
                  type="text"
                  name="username"
                  id="username"
                  placeholder="name@company.com"
                  required
                  labelFor={"username"}
                  value={username}
                  onChange={handleUsernameChange}
                />
              </div>
              <div>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="••••••••"
                  required
                  value={password}
                  onChange={handlePasswordChange}
                  labelFor={"password"}
                  label={"Password"}
                />
              </div>
              <Button label={"Sign in"} onClick={handleSubmit} />
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AuthForm;
