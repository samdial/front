import React from "react";
import Button from "../../../shared/ui/Button/ui/Button";
import { useAuth } from "../../../app/porviders/AuthProvider/ui/AuthContext";

export const LogoutButton = () => {
  const { logout } = useAuth();
  return <Button label={"Logout"} onClick={logout} />;
};
