import React from "react";
import { AppPage } from "../../../widgets/AppPage/ui/AppPage";

const LobbyPage: React.FC = () => {
  return (
    <AppPage>
      <div className={"flex items-center"}>
        <h1 className={"text-3xl font-bold underline"}>
          Добро пожаловать в лобби!
        </h1>
      </div>
    </AppPage>
  );
};

export default LobbyPage;
