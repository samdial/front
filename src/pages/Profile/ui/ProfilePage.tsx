import React, { useState, useEffect } from "react";
import { Loader } from "../../../shared/ui/Loader/ui/Loader";
import { AppPage } from "../../../widgets/AppPage/ui/AppPage";

interface User {
  username: string;
  email: string;
}

const ProfilePage: React.FC = () => {
  const [user, setUser] = useState<User | null>(null); // Используем состояние для хранения информации о пользователе

  // логика для загрузки данных о пользователе, например, из API

  const fetchUserData = async () => {
    // Здесь происходит запрос к  API для получения данных о пользователе
    //
    const response = await fetch("/api/user");
    const userData = await response.json();
    setUser(userData);
  };

  // Загружаем данные пользователя при монтировании компонента
  useEffect(() => {
    fetchUserData();
  }, []);

  return (
    <AppPage>
      <div className={"flex items-center justify-center w-full h-full"}>
        {!user ? (
          <Loader />
        ) : (
          <>
            <h2>Профиль</h2>
            <p>Имя пользователя: {user.username}</p>
            <p>Email: {user.email}</p>
          </>
        )}
      </div>
    </AppPage>
  );
};

export default ProfilePage;
