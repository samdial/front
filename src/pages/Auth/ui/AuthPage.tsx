import React, { useEffect } from "react";
import AuthForm from "../../../features/Login/ui/AuthForm";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../../app/porviders/AuthProvider/ui/AuthContext";

export const AuthPage = () => {
  const navigate = useNavigate();
  const { userAuthData } = useAuth();

  useEffect(() => {
    if (userAuthData) {
      navigate("/");
    }
  }, [userAuthData, navigate]);

  return (
    <div>
      <AuthForm />
    </div>
  );
};
