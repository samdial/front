import React, { useState } from 'react';

export interface GameLogicState {
    board: Array<string | null>;
    nextPlayer: 'X' | 'O';
}
export const usxGameLogic = (): [GameLogicState, (index: number) => void] => {
    const [state, setState] = useState<GameLogicState>({
        board: Array(9).fill(null),
        nextPlayer: 'X',
    });

    const handleClick = (index: number): void => {
        const { board, nextPlayer } = state;
        if (board[index] === null) {
            const newBoard = [...board];
            newBoard[index] = nextPlayer;
            setState({
                board: newBoard,
                nextPlayer: nextPlayer === 'X' ? 'O' : 'X',
            });

            // Отправить API запрос
            sendAPICall(index);
        }
    };

    const sendAPICall = (index: number) => {
        // Здесь написать код для отправки API запроса

        console.log('Отправлен API запрос для индекса:', index);
    };

    return [state, handleClick];
};