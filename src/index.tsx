import React from "react";
import ReactDOM from "react-dom/client";
import "./app/styles/reset.css";
import "./app/styles/index.css";
import App from "./app/App/ui/App";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
