import React, { FC, PropsWithChildren } from "react";
import { useAuth } from "../../../app/porviders/AuthProvider/ui/AuthContext";
import { useNavigate } from "react-router-dom";

export const AppPage: FC<PropsWithChildren> = ({ children }) => {
  const { isLoggedIn } = useAuth();
  const navigate = useNavigate();

  if (!isLoggedIn) {
    navigate("/auth");
  }

  return <>{children}</>;
};
