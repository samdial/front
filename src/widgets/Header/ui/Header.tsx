import React from "react";
import { Link } from "react-router-dom";
import { AppLink } from "../../../shared/ui/AppLink/ui/AppLink";
import { useAuth } from "../../../app/porviders/AuthProvider/ui/AuthContext";
import { LogoutButton } from "../../../features/Logout/ui/LogoutButton";

export const Header = () => {
  const { isLoggedIn } = useAuth();
  return (
    <header>
      <nav className="bg-white border-gray-200 px-4 lg:px-6 py-2.5 dark:bg-gray-800">
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
          <Link to="/" className="flex items-center">
            <img
              src="https://flowbite.com/docs/images/logo.svg"
              className="mr-3 h-6 sm:h-9"
              alt="Flowbite Logo"
            />
            <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
              Крестики нолики
            </span>
          </Link>
          <div
            className="hidden justify-between items-center w-full lg:flex lg:w-auto lg:order-1"
            id="mobile-menu-2"
          >
            <ul className="flex flex-col items-center mt-4 font-medium lg:flex-row lg:space-x-8 lg:mt-0">
              {isLoggedIn && (
                <>
                  <li>
                    <AppLink to={"/"} label={"Lobby"} />
                  </li>
                  <li>
                    <AppLink to={"/profile"} label={"Profile"} />
                  </li>
                  <li>
                    <LogoutButton />
                  </li>
                </>
              )}
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};
