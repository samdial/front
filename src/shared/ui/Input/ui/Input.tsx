import React, { FC, InputHTMLAttributes, useMemo } from "react";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  labelFor?: string;
}

const Input: FC<InputProps> = ({ label, labelFor, className, ...other }) => {
  const controlClassNames = useMemo(() => {
    const classes = [
      "bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500",
    ];

    if (className) {
      classes.push(className);
    }

    return classes.join(" ");
  }, [className]);
  return (
    <>
      {label && (
        <label
          htmlFor={labelFor}
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
        >
          {label}
        </label>
      )}
      <input {...other} className={controlClassNames} />
    </>
  );
};

export default Input;
